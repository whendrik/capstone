# Notebooks submittions for Capstone Project

https://www.edx.org/course/data-science-and-machine-learning-capstone-project

## Packages I found while reviewing other

- missingno
- https://imbalanced-learn.readthedocs.io/en/stable/

## Folium Preview broken on gitlab

Use the following jupyter service to see the folium plots

https://nbviewer.jupyter.org/

